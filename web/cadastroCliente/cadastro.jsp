
<%@page import="java.util.List"%>
<%@page import="br.com.senac.banco.CidadeDAO"%>
<%@page import="br.com.senac.modelo.Cidade"%>
<jsp:include page="../header.jsp" />

<% CidadeDAO cidadeDAO = new CidadeDAO();

    List<Cidade> listaCidade = cidadeDAO.listarTodos();

%>



<div class="container">
    <fieldset>
        <legend>Cadastro de Cliente</legend>
        <form class="form-horizontal">
            <div class="panel panel-default">
                <div class="panel-heading">Dados Pessoais</div>

                <br />

                <input type="hidden" name="codigo" value="0">

                <div class="form-group">
                    <label class="control-label col-sm-2" for="codigo">C�digo:</label>
                    <div class="col-sm-2">
                        <input readonly="true" class="form-control" id="codigo">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="primeiroNome">Primeiro nome:</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="primeiroNome" placeholder="Entre com o primeiro nome" name="primeiroNome" required="true">
                    </div>

                    <label class="control-label col-sm-2" for="ultimoNome">Ultimo nome: </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="ultimoNome" placeholder="Entre com o Ultimo nome" name="ultimoNome">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="">E-mail:</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="email" placeholder="someone@example.com" name="email">                
                    </div>
                </div>            

            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Endere�o</div>
                <div class="panel-body">


                    <div class="form-group">
                        <label class="control-label col-sm-2" for="endereco">Logradouro:</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="endereco" name="endereco">
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="complemento">Complemento:</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="complemento" placeholder="Complemento" name="complemento">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label col-sm-2" for="bairro">Bairro: </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="bairro" placeholder="bairro" name="bairro">
                        </div>

                        <label class="control-label col-sm-2" for="cep">CEP: </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="cep" placeholder="cep" name="cep">
                        </div>

                    </div>

                    <br />


                    <div class="form-group">

                        <label class="control-label col-sm-2" for="cidade">Cidade:</label>
                        <div class="col-sm-2"> 
                            <select class="form-control " id="cidade" name="cidade">
                                <option value="0">Cidade</option>

                                <% for (Cidade c : listaCidade) {%>

                                <option value="<%= c.getCodigo()%>" ><%= c.getNome()%></option>

                                <% }%>

                            </select>
                        </div>

                        <label class="control-label col-sm-1" for="codigoCidade" >C�digo:</label>
                        <div class="col-sm-1">
                            <input readonly="true" class="form-control" id="codigoCidade" placeholder="codigoCidade" name="codigoCidade">
                        </div>

                        <label class="control-label col-sm-2" for="Pais">Pais:</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="Pais" placeholder="Pais" name="Pais">
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </form>



        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-12">
                <div class="row">
                    <div class="col-sm-6 col-xs-12"> 
                        <input type="submit"   class="btn btn-primary col-" value="Salvar" />
                        <input type="reset"    class="btn btn-danger" value="Cancelar" />
                    </div>
                </div>

            </div>
        </div>


    </fieldset>


</div>



<%
    if (request.getAttribute("mensagem") != null) {
        out.print(request.getAttribute("mensagem"));
    } else {
        out.print("");
    }
%>


<jsp:include page="../footer.jsp" />
