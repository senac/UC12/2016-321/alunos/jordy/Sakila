package br.com.senac.banco;

import br.com.senac.modelo.Ator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class AtorDAO implements DAO<Ator> {

    @Override
    public void salvar(Ator ator) {

        Connection connection = null;

        try {

            connection = Conexao.getConnection();

            String query = "INSERT INTO actor (FIRST_NAME , lAST_NAME) VALUES ( ? , ?) ; ";

            PreparedStatement ps = connection.prepareStatement(query);

            ps.setString(1, ator.getPrimeiroNome());
            ps.setString(2, ator.getUltimoNome());

            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println("Falha ao salvar");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao");
            }
        }

    }

    @Override
    public void atualizar(Ator ator) {

        Connection connection = null;

        try {

            connection = Conexao.getConnection();

            String query = "UPDATE ACTOR "
                    + "SET first_name = ? , last_name= ? "
                    + "WHERE actor_id = ? ; ";

            PreparedStatement ps = connection.prepareStatement(query);

            ps.setString(1, ator.getPrimeiroNome());
            ps.setString(2, ator.getUltimoNome());
            ps.setInt(3, ator.getCodigo());

            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println("Falha ao salvar");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao");
            }
        }

    }

    @Override
    public void deletar(int id) {

        Connection connection = null;
        try {

            connection = Conexao.getConnection();

            String query = "DELETE FROM actor WHERE actor_id = ? ; ";

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);

            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println("Falha ao salvar");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao");
            }
        }
    }

    @Override
    public List<Ator> listarTodos() {

        List<Ator> lista = new ArrayList<>();

        Connection connection = null;
        try {

            connection = Conexao.getConnection();

            String query = "SELECT * FROM actor; ";

            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery(query);
            
            while(rs.next()){
                
                int codigo =  rs.getInt("actor_id") ; 
                String primeiroNome = rs.getString("first_name") ; 
                String ultimoNome = rs.getString("last_name") ; 
                
                Ator ator = new Ator(codigo, primeiroNome, ultimoNome);
                
                lista.add(ator);
            }
            

        } catch (Exception ex) {
            System.out.println("Falha ao salvar");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao");
            }
        }

        return lista;

    }

}
