/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.banco;

import br.com.senac.modelo.Cidade;
import br.com.senac.modelo.Pais;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jordy.allan
 */
public class CidadeDAO implements DAO<Cidade>{

    @Override
    public void salvar(Cidade cidade) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void atualizar(Cidade cidade) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletar(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Cidade> listarTodos() {
         List<Cidade> lista = new ArrayList<>();

        Connection connection = null;

        try {
            connection = Conexao.getConnection();

            String query = "SELECT * FROM city c inner join country co on c.country_ID = co.country_ID ; ";

            Statement st = connection.createStatement();

            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {

                Pais pais = new Pais(rs.getInt("country_ID"), rs.getString("country"));

                Cidade cidade = new Cidade(rs.getInt("city_ID"), rs.getString("city"), pais);

                lista.add(cidade);
            }

        } catch (SQLException ex) {
            System.err.println("Erro ao atualizar ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar o banco");
            }
        }

        return lista;
    }
    
    public Cidade buscarPorId(int id) {
        
        Connection connection = null;
        Cidade cidade = null;
        
        try{
            
            connection = Conexao.getConnection();
            
            String query = "SELECT * FROM city c inner join country co "
                    + "on c.country_ID = co.country_ID where city_ID = ?  ; ";
            
            PreparedStatement ps = connection.prepareStatement(query);
            
            ResultSet rs = ps.executeQuery();
            
            if (rs.first()){
                Pais pais = new Pais(rs.getInt("country_ID"), rs.getString("country"));

                cidade = new Cidade(rs.getInt("city_ID"), rs.getString("city"), pais);
            }
            } catch (SQLException ex) {
            System.err.println("Erro ao atualizar ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar o banco...");
            }
        }

        return cidade;
 
        }
        
    }
    

