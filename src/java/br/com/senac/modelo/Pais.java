package br.com.senac.modelo;

public class Pais {

    private int codigo;
    private String nome;

    public Pais() {
    }

    public Pais(int codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }
   

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
